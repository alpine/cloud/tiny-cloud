# PACKAGING

## Alpine Linux

The packaging of Tiny Cloud APKs for Alpine Linux can be found at
https://gitlab.alpinelinux.org/alpine/aports/-/tree/master/main/tiny-cloud

As of **3.1.0**, the main `tiny-cloud` APK contains all cloud and user-data
modules, and set `CLOUD="auto"` in `/etc/tiny-cloud.conf`.  The per-cloud
subpackages configure `/etc/tiny-cloud.conf` for that particular cloud, and the
cloud subpackage installed _last_ is the one that "wins".

Before 3.1.0, each per-cloud subpackage also provided its own cloud-specific
libs.
