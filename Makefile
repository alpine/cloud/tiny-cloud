PREFIX ?= /

CLOUDS = aws azure gcp hetzner incus nocloud oci scaleway

SUBPACKAGES = core openrc $(CLOUDS)

.PHONY: check install $(SUBPACKAGES)

install: $(SUBPACKAGES)

core:
	install -Dm755 -t "$(PREFIX)"/usr/bin \
		bin/imds
	install -Dm644 -t "$(PREFIX)"/usr/lib/tiny-cloud \
		lib/tiny-cloud/common \
		lib/tiny-cloud/init \
		lib/tiny-cloud/tiny-cloud.conf
	install -Dm644 -t "$(PREFIX)"/usr/lib/tiny-cloud/user-data \
		lib/tiny-cloud/user-data/*
	install -Dm644 lib/tiny-cloud/tiny-cloud.conf \
		"$(PREFIX)"/etc/tiny-cloud.conf
	install -Dm755 -t "$(PREFIX)"/usr/sbin \
		sbin/tiny-cloud

openrc:
	install -Dm755 -t "$(PREFIX)"/etc/init.d \
		dist/openrc/*

$(CLOUDS):
	install -Dm644 -t "$(PREFIX)"/usr/lib/tiny-cloud/cloud/$@ \
		lib/tiny-cloud/cloud/$@/*
	[ -f "$(PREFIX)"/usr/lib/tiny-cloud/cloud/$@/autodetect ] && \
		chmod 0755 "$(PREFIX)"/usr/lib/tiny-cloud/cloud/$@/autodetect

check: tests/Kyuafile Kyuafile
	kyua --variable parallelism=$(shell nproc) test || (kyua report --verbose && exit 1)

tests/Kyuafile: $(wildcard tests/*.test)
	echo "syntax(2)" > $@.tmp
	echo "test_suite('tiny-cloud')" >> $@.tmp
	for i in $(notdir $(wildcard tests/*.test)); do \
		echo "atf_test_program{name='$$i',timeout=5}" >> $@.tmp ; \
	done
	mv $@.tmp $@

Kyuafile:
	echo "syntax(2)" > $@.tmp
	echo "test_suite('tiny-cloud')" >> $@.tmp
	echo "include('tests/Kyuafile')" >> $@.tmp
	mv $@.tmp $@
