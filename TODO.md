# TODO

## SOON-ish

* Set up ephemeral networks early for the clouds that have network-accessible
  IMDS, so we can get at user-data sooner than later.

* Support multiple network IMDS endpoints (ie. both IPv6 & IPv4).

* Test improvements - each user-data handler should be tested with each cloud.

* `man` pages for `imds` and `tiny-cloud`.

## FUTURE

* Support `vendor-data`?  In theory this is a baseline, and and `user-data`
  is layered on top of that.

* Support additional features of `#cloud-config` as needed

* Support LVM partitioning and non-`ext[234]` filesystems?

* Support other cloud providers...
  * Digital Ocean
  * IBM
  * Openstack
  * ???
